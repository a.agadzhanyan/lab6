<!DOCTYPE html>
<html lang="ru">

  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Задание 5</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      <script src="script.js"></script>
      <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error_text {
    color: darkred;
}
    </style>
  </head>
  <body>
      <main>
          <div class="container">
              <div class="row">
                  <div class="col-md-6 offset-md-3">
                        <form action="" method="POST">
                            <div class="form-group row">
                                <label>Имя</label>
                                <input class="form-control" name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
                                <?php if ($errors['fio']) {print $messages[0];} ?>
                            </div>

                            <div class="form-group row">
                                <label>E-mail</label>
                                <input class="form-control" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" />
                                <?php if ($errors['email']) {print $messages[1];} ?>
                            </div>
                            <div class="form-group row">
                                <label>Дата рождения</label>
                                <input class="form-control" name="birthday" <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>" type="date" min="1900-01-01" max="2022-12-31">
                                <?php if ($errors['birthday']) {print $messages[2];} ?>
                            </div>

                            <div class="form-group row">
                                <label>Гендер</label>
                                <?php if ($errors['gender']) {print '<div class="error">';} ?>
                                <div class="form-check">
                                    <input type="radio" name="gender" value="Male" <?php if($values['gender']=='Male') print "checked";else print ""; ?> />
                                    <label>мужской</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" name="gender" value="Female" <?php echo ($values['gender']=='Female') ?  "checked" : "" ;  ?> />
                                    <label>женский</label>
                                </div>
                                <?php if ($errors['gender']) {print '</div>';print $messages[3];} ?>
                            </div>

                            <fieldset class="form-group row">
                                <div class="row">
                                    <label class="col-form-label col-sm-2 pt-0">Количество конечностей</label>
                                    <?php if ($errors['limbs']) {print '<div class="error">';} ?>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input type="radio" name="limbs" value="0" <?php echo ($values['limbs']=='0') ?  "checked" : "" ;  ?>/>
                                            <label for="gridRadios1">Полный человеческий комплект</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" name="limbs" value="1" <?php echo ($values['limbs']=='1') ?  "checked" : "" ;  ?>/>
                                            <label for="gridRadios2">Немного инопланетянин</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" name="limbs" value="2" <?php echo ($values['limbs']=='2') ?  "checked" : "" ;  ?>/>
                                            <label for="gridRadios3">Для жизни хватает</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" name="limbs" value="3" <?php echo ($values['limbs']=='3') ?  "checked" : "" ;  ?>/>
                                            <label for="gridRadios4">Найти бы еще парочку рук</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" name="limbs" value="4" <?php echo ($values['limbs']=='4') ?  "checked" : "" ;  ?>/>
                                            <label for="gridRadios5">Бесконечностей</label>
                                        </div>
                                        <?php if ($errors['limbs']) {print '</div>';print $messages[4];} ?>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="form-group row">
                                <label for="Superpowers">Сверхспособности</label>
                                <select class="form-control" name="ability[]"
                                        multiple="multiple" <?php if ($errors['ability']) {print 'class="error"';} ?>>
                                    <option value="0" <?php echo (in_array("0",$ability)) ?  "selected" : ""  ; ?>>Кодить без багов</option>
                                    <option value="1" <?php echo (in_array('1',$ability)) ?  "selected" : ""  ; ?>>Читать мысли животных</option>
                                    <option value="2" <?php echo (in_array('2',$ability)) ?  "selected" : ""  ; ?>>Не спать(мечта студента =))</option>
                                </select>
                                <?php if ($errors['ability']) {print $messages[5];} ?>
                            </fieldset>

                            <div class="form-group row">
                                <label for="comment">Комментарий</label>
                                <textarea rows="3" class="form-control" name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php echo $values['biography'] ;  ?></textarea>
                                <?php if ($errors['biography']) {print $messages[6];} ?>
                            </div>

                            <div class="form-group form-check row">
                                <input type="checkbox" class="form-check-input" name="contract" <?php if ($errors['contract']) {print 'class="error"';} ?>/>
                                <label class="form-check-label" for="agree">Соглашаюсь с обработкой персональных данных</label>
                                <?php if ($errors['contract']) {print $messages[7];} ?>
                            </div>

                            <div class="row">
                                <button type="submit" id="submit" value="ok" class="btn btn-dark btn-lg btn-block">Отправить</button>
                                <!--                        <a class="btn btn-dark btn-lg btn-block" href="login.php">Войти</a>-->

                                <a class="btn btn-dark btn-lg btn-block" href="admin.php">Админ</a>
                                <?php
                                if (!empty($messages)) {
                                    if(!empty($_SESSION['login'])){
                                        print '<a class="btn btn-dark btn-lg btn-block" href="login.php">Выйти</a>';
                                        printf($messages[10], $_SESSION['login']);
                                    } else {
                                        print '<a class="btn btn-dark btn-lg btn-block" href="login.php">Войти</a>';
                                    };
                                }
                                print $messages[8];
                                print $messages[9];
                                ?>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </main>
  </body>

</html>
